using Sirenix.OdinInspector;
using TestProject.Character.Implementation;
using TestProject.Data.Implementation;
using TestProject.Store.Implementation;
using TestProject.User.Implementation;
using UnityEngine;
using Zenject;

public class StoreSceneInstaller : MonoInstaller
{
    [SerializeField, Required] private TextAsset _dataXml = default;

    public override void InstallBindings()
    {
        Container
            .BindInterfacesAndSelfTo<StoreTabButtonContainerView>().
            FromComponentInHierarchy(true)
            .AsSingle()
            .NonLazy();

        Container
            .BindInterfacesAndSelfTo<StoreItemContainerView>()
            .FromComponentInHierarchy(true)
            .AsSingle()
            .NonLazy();

        Container
            .BindInterfacesAndSelfTo<StoreController>()
            .AsSingle()
            .NonLazy();

        Container
            .BindInterfacesAndSelfTo<StoreObjectDataContainer>()
            .FromInstance(new StoreObjectDataContainer(_dataXml.text))
            .AsSingle()
            .NonLazy();

        Container
            .BindInterfacesAndSelfTo<UserLevelDisplay>()
            .FromComponentInHierarchy(true)
            .AsSingle()
            .NonLazy();

        Container
            .BindInterfacesAndSelfTo<UserCoinDisplay>()
            .FromComponentInHierarchy(true)
            .AsSingle()
            .NonLazy();

        Container
            .BindInterfacesAndSelfTo<UserController>()
            .AsSingle()
            .NonLazy();

        Container
            .BindInterfacesAndSelfTo<CharacterView>()
            .FromComponentInHierarchy(true)
            .AsSingle()
            .NonLazy();

        Container
            .BindInterfacesAndSelfTo<CharacterPointView>()
            .FromComponentsInHierarchy(includeInactive: true)
            .AsCached()
            .NonLazy();
    }
}