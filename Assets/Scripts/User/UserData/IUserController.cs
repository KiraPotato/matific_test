﻿namespace TestProject.User
{
    public interface IUserController
    {
        bool HasEnoughCoins(int requiredAmount);
        bool IsHighEnoughLevel(int requiredLevel);
        bool TryDeductCoins(int cost, int requiredLevel);
    }
}