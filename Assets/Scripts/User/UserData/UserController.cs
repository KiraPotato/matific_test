namespace TestProject.User.Implementation
{
    public class UserController : IUserController
    {
        private IUserLevelDisplay _levelDisplay;
        private IUserCoinDisplay _coinDisplay;

        private UserData _userData;

        public UserController(IUserLevelDisplay levelDisplay, IUserCoinDisplay coinDisplay)
        {
            _userData = UserDataGetter.GetUserData();
            _levelDisplay = levelDisplay;
            _coinDisplay = coinDisplay;

            _levelDisplay.UpdateLevel(_userData.Level);
            _coinDisplay.UpdateCoinCount(_userData.Coins);
        }

        public bool HasEnoughCoins(int requiredAmount)
            => _userData.Coins >= requiredAmount;

        public bool IsHighEnoughLevel(int requiredLevel)
            => _userData.Level >= requiredLevel;

        public bool TryDeductCoins(int cost, int requiredLevel)
        {
            if (HasEnoughCoins(cost) && IsHighEnoughLevel(requiredLevel))
            {
                _userData.Coins -= cost;
                _coinDisplay.UpdateCoinCount(_userData.Coins);
                return true;
            }

            return false;
        }
    }
}