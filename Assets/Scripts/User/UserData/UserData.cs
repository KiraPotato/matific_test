namespace TestProject.User.Implementation
{
    public class UserData
    {
        public int Level { get; set; }
        public int Coins { get; set; }
    }
}