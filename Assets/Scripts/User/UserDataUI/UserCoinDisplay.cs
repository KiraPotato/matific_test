﻿using UnityEngine;
using TMPro;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

namespace TestProject.User.Implementation
{
    public class UserCoinDisplay : MonoBehaviour, IUserCoinDisplay
    {
        [SerializeField] private TMP_Text _coinAmount = default;
        [SerializeField] private float _cointChangeTime = .5f;

        private int _lastAmount = 0;
        TweenerCore<int, int, NoOptions> _tween;

        private void Awake()
        {
            if (NullChecker.IsNull(_coinAmount))
                Logger.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
        }

        public void UpdateCoinCount(int newAmount)
        {
            if (NullChecker.IsNotNull(_tween))
            {
                _tween.Kill();
            }

            int amount = _lastAmount;
            _lastAmount = newAmount;

            int Getter() => amount;

            void Setter(int value)
            {
                amount = value;
                _coinAmount.text = amount.ToString();
            }

            _tween = DOTween.To(Getter, Setter, newAmount, _cointChangeTime);

            _tween.onComplete -= OnTweenEnd;
            _tween.onComplete += OnTweenEnd;

            _tween.onKill -= OnTweenEnd;
            _tween.onKill += OnTweenEnd;

            _coinAmount.text = newAmount.ToString();
        }

        private void OnTweenEnd()
        {
            _tween = null;
        }
    }
}