﻿namespace TestProject.User
{
    public interface IUserCoinDisplay
    {
        void UpdateCoinCount(int newAmount);
    }
}