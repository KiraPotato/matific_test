﻿using TMPro;
using UnityEngine;

namespace TestProject.User.Implementation
{
    public class UserLevelDisplay : MonoBehaviour, IUserLevelDisplay
    {
        [SerializeField] private TMP_Text _levelText = default;

        private void Awake()
        {
            if (NullChecker.IsNull(_levelText))
                Logger.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
        }

        public void UpdateLevel(int newLevel)
            => _levelText.text = newLevel.ToString();
    }
}