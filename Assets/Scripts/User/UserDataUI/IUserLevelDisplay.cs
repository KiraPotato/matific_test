﻿namespace TestProject.User
{
    public interface IUserLevelDisplay
    {
        void UpdateLevel(int newLevel);
    }
}