using System.Collections.Generic;
using UnityEngine;
using Zenject;

using TestProject.Data;

namespace TestProject.Character.Implementation
{
    public class CharacterView : MonoBehaviour, ICharacterView
    {
        private Dictionary<string, CharacterPointView> _locations;

        [Inject]
        private void Initialize(CharacterPointView[] views)
        {
            _locations = new Dictionary<string, CharacterPointView>();

            foreach (CharacterPointView item in views)
            {
                _locations[item.Section] = item;

                item.HideSprite();
            }
        }

        public void ApplyObject(string section, IStoreObject storeObject = null)
        {
            if (_locations.ContainsKey(section))
            {
                if (NullChecker.IsNull(storeObject))
                {
                    _locations[section].HideSprite();
                    return;
                }

                storeObject.AsyncFetchWorldSprite((sprite) => _locations[section].ShowSprite(sprite));
            }
        }
    }
}