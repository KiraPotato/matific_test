﻿using TestProject.Data;

namespace TestProject.Character
{
    public interface ICharacterView
    {
        void ApplyObject(string section, IStoreObject storeObject = null);
    }
}