﻿using UnityEngine;
using UnityEngine.UI;

namespace TestProject.Character.Implementation
{
    [RequireComponent(typeof(Image), typeof(CanvasGroup))]
    public class CharacterPointView : MonoBehaviour
    {
        public string Section => _section;

        [SerializeField] private string _section = default;

        private Image _image;
        private CanvasGroup _canvasGroup;

        private void Awake()
        {
            if (string.IsNullOrWhiteSpace(_section))
                Logger.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
        }

        public void ShowSprite(Sprite sprite)
        {
            GetComponents();

            _image.sprite = sprite;
            _canvasGroup.Show();
        }

        public void HideSprite()
        {
            GetComponents();

            _image.sprite = null;
            _canvasGroup.Hide();
        }

        private void GetComponents()
        {
            if (NullChecker.IsNull(_image))
                _image = GetComponent<Image>();

            if (NullChecker.IsNull(_canvasGroup))
                _canvasGroup = GetComponent<CanvasGroup>();
        }
    }
}