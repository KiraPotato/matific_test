﻿using System.Collections.Generic;

using TestProject.Character;
using TestProject.Data;
using TestProject.Data.Implementation;
using TestProject.User;

namespace TestProject.Store.Implementation
{
    public class StoreController
    {
        private IStoreTabButtonContainerView _tabContainer;
        private IStoreItemContainerView _itemContainer;
        private IStoreObjectsContainer _storeObjectsContainer;
        private IUserController _userController;
        private ICharacterView _characterView;

        public StoreController(
            IStoreItemContainerView itemContainer,
            IStoreTabButtonContainerView tabContainer,
            IStoreObjectsContainer storeObjectsContainer,
            IUserController userController,
            ICharacterView characterView)
        {
            _itemContainer = itemContainer;
            _tabContainer = tabContainer;
            _storeObjectsContainer = storeObjectsContainer;

            tabContainer.OnTabButtonClicked += OnTabButtonClicked;
            tabContainer.Initialize(storeObjectsContainer.StoreObjects.Keys);

            itemContainer.OnStoreTabReady += OnStoreTabReady;
            itemContainer.OnStoreObjectClicked += OnStoreItemClicked;
            itemContainer.Initialize(IsItemLocked, IsItemPurchasable, IsItemButtonInteractable);

            _userController = userController;
            _characterView = characterView;
        }

        ~StoreController()
        {
            _tabContainer.OnTabButtonClicked -= OnTabButtonClicked;
            _itemContainer.OnStoreObjectClicked -= OnStoreItemClicked;
            _itemContainer.OnStoreTabReady -= OnStoreTabReady;
        }

        private void OnTabButtonClicked(string tabId)
        {
            _tabContainer.SetActiveTab(tabId);

            IReadOnlyList<StoreObjectData> entries = _storeObjectsContainer.StoreObjects[tabId];
            _itemContainer.ShowEntries(entries);
        }

        private void OnStoreItemClicked(IStoreObject clickedObject)
        {
            if (clickedObject.Owned)
            {
                _characterView.ApplyObject(clickedObject.Section, clickedObject);
                return;
            }

            if (_userController.TryDeductCoins(clickedObject.Price, clickedObject.MinLevel))
            {
                clickedObject.ChangeOwnedState(isOwned: true);
                _itemContainer.RefreshButtons();
            }
        }

        private void OnStoreTabReady()
        {
            var list = new List<string>(_storeObjectsContainer.StoreObjects.Keys);

            OnTabButtonClicked(list[0]);
        }

        private bool IsItemButtonInteractable(IStoreObject item)
        {
            if (IsItemLocked(item))
                return false;
            else if (IsItemPurchasable(item) && _userController.HasEnoughCoins(item.Price) == false)
                return false;
            else
                return true;
        }

        private bool IsItemPurchasable(IStoreObject item)
            => item.Owned == false && item.Price > 0;

        private bool IsItemLocked(IStoreObject item)
            => _userController.IsHighEnoughLevel(item.MinLevel) == false;
    }
}