﻿using System.Collections.Generic;

using TestProject.Store.Implementation;

public interface IStoreTabButtonContainerView
{
    event OnTabButtonClickedDelegate OnTabButtonClicked;

    void Initialize(IEnumerable<string> tabIds);
    void SetActiveTab(string tabId);
}