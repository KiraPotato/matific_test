using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

using TestProject.Data;

namespace TestProject.Store.Implementation
{
    public class StoreItemView : SerializedMonoBehaviour
    {
        public event OnStoreObjectClickedDelegate OnStoreObjectClicked;

#pragma warning disable 649
        [SerializeField] private Button _button;
        [SerializeField] private Image _icon;
        [SerializeField] private CanvasGroup _lockIcon;
        [SerializeField] private CanvasGroup _priceIcon;
        [SerializeField] private TMP_Text _priceText;
        [SerializeField] private TMP_Text _requiredLevelText;
#pragma warning restore 649

        private IStoreObject _targetObject;

        private void Awake()
        {
            if (NullChecker.IsAnyNull(_button, _icon, _lockIcon, _priceIcon, _priceText, _requiredLevelText))
                Logger.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
            else
                _button.onClick.AddListener(OnButtonClicked);
        }

        public void Initialize(IStoreObject targetObject, bool isLocked, bool isPurchasable, bool buttonInteractable)
        {
            _targetObject = targetObject;

            targetObject.AsyncFetchIconSprite(OnIconLoaded);

            _priceText.text = targetObject.Price.ToString();
            _requiredLevelText.text = targetObject.MinLevel.ToString();

            ChangeIsLocked(isLocked);
            ChangeIsPurchasable(isPurchasable);
            ChangeButtonInteractability(buttonInteractable);
        }

        public void ChangeIsLocked(bool isLocked)
            => _lockIcon.ChangeVisibility(isLocked);

        public void ChangeIsPurchasable(bool isPurchasable)
            => _priceIcon.ChangeVisibility(isPurchasable);

        public void ChangeButtonInteractability(bool isInteractable)
            => _button.interactable = isInteractable;

        private void OnButtonClicked()
        {
            OnStoreObjectClicked?.Invoke(_targetObject);
        }

        private void OnIconLoaded(Sprite sprite)
        {
            _icon.sprite = sprite;
        }

    }

    public delegate void OnStoreObjectClickedDelegate(IStoreObject clickedObject);
}