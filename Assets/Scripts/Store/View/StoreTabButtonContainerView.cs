using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace TestProject.Store.Implementation
{
    public class StoreTabButtonContainerView : MonoBehaviour, IStoreTabButtonContainerView
    {
        public event OnTabButtonClickedDelegate OnTabButtonClicked;

#pragma warning disable 649
        [SerializeField] private string _tabButtonAddress;
        [SerializeField] private Transform _tabButtonContainer;
#pragma warning restore 649

        private Dictionary<string, StoreTabButtonView> _tabButtons;
        private string _previouslyActiveTabId;

        private void Awake()
        {
            if (string.IsNullOrWhiteSpace(_tabButtonAddress) || NullChecker.IsNull(_tabButtonContainer))
                Logger.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
        }

        public void Initialize(IEnumerable<string> tabIds)
        {
            _tabButtons = new Dictionary<string, StoreTabButtonView>();

            AsyncOperationHandle<GameObject> handler = 
                Addressables.LoadAssetAsync<GameObject>(_tabButtonAddress);

            handler.Completed += onAssetLoaded;

            void onAssetLoaded(AsyncOperationHandle<GameObject> obj)
            {
                foreach (string tabId in tabIds)
                {
                    GameObject go = Instantiate(obj.Result, _tabButtonContainer);
                    StoreTabButtonView tabView = go.GetComponent<StoreTabButtonView>();

                    tabView.Initialize(tabId, OnButtonClick);
                    _tabButtons[tabId] = tabView;
                }
            }
        }

        public void SetActiveTab(string tabId)
        {
            if (string.IsNullOrWhiteSpace(_previouslyActiveTabId) == false)
                _tabButtons[_previouslyActiveTabId].SetInteractable(true);

            if (_tabButtons.ContainsKey(tabId))
            {
                _tabButtons[tabId].SetInteractable(false);
                _previouslyActiveTabId = tabId;
            }
        }

        private void OnButtonClick(string tabId)
        {
            OnTabButtonClicked?.Invoke(tabId);
        }
    }

    public delegate void OnTabButtonClickedDelegate(string tabId);
}