using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

using TestProject.Data;

namespace TestProject.Store.Implementation
{
    public class StoreItemContainerView : MonoBehaviour, IStoreItemContainerView
    {
        public event OnStoreObjectClickedDelegate OnStoreObjectClicked;
        public event OnStoreTabReadyDelegate OnStoreTabReady;

#pragma warning disable 649
        [SerializeField] private string _displayPrefabAddress;
#pragma warning restore 649

        private MonoBehaviourPooling<StoreItemView> _storeItemPooler;
        private Dictionary<IStoreObject, StoreItemView> _currentItems;
        private IsItemLockedDelegate _isItemLockedCheck;
        private IsItemPurchasableDelegate _isItemPurchasableCheck;
        private IsItemButtonInteractableDelegate _isItemButtonInteractableCheck;

        private void Awake()
        {
            if (string.IsNullOrWhiteSpace(_displayPrefabAddress))
                Logger.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
        }

        public void Initialize(
            IsItemLockedDelegate isItemLockedCheck,
            IsItemPurchasableDelegate isItemPurchasableCheck,
            IsItemButtonInteractableDelegate isItemButtonInteractableCheck)
        {
            AsyncOperationHandle<GameObject> handler =
                Addressables.LoadAssetAsync<GameObject>(
                    _displayPrefabAddress);

            handler.Completed -= OnPrefabLoaded;
            handler.Completed += OnPrefabLoaded;

            _isItemLockedCheck = isItemLockedCheck;
            _isItemPurchasableCheck = isItemPurchasableCheck;
            _isItemButtonInteractableCheck = isItemButtonInteractableCheck;
        }

        public void ShowEntries(IReadOnlyList<IStoreObject> entries)
        {
            if (NullChecker.IsNull(_currentItems))
                _currentItems = new Dictionary<IStoreObject, StoreItemView>();

            ClearCurrentItems();

            foreach (IStoreObject storeObject in entries)
            {
                StoreItemView view = _storeItemPooler.FetchObject(storeObject.Name);
                _currentItems[storeObject] = view;

                view.OnStoreObjectClicked -= OnButtonClicked;
                view.OnStoreObjectClicked += OnButtonClicked;

                bool isLocked = _isItemLockedCheck(storeObject);
                bool isPurchasable = _isItemPurchasableCheck(storeObject);
                bool isButtonInteractable = _isItemButtonInteractableCheck(storeObject);

                view.Initialize(storeObject, isLocked, isPurchasable, isButtonInteractable);
            }
        }

        public void RefreshButtons()
        {
            foreach (var item in _currentItems)
            {
                StoreItemView view = item.Value;
                IStoreObject data = item.Key;

                view.ChangeButtonInteractability(_isItemButtonInteractableCheck(data));
                view.ChangeIsLocked(_isItemLockedCheck(data));
                view.ChangeIsPurchasable(_isItemPurchasableCheck(data));
            }
        }

        private void OnButtonClicked(IStoreObject clickedObject)
        {
            OnStoreObjectClicked?.Invoke(clickedObject);
        }

        private void ClearCurrentItems()
        {
            foreach (StoreItemView item in _currentItems.Values)
            {
                item.OnStoreObjectClicked -= OnButtonClicked;
                _storeItemPooler.PoolObject(item);
            }

            _currentItems.Clear();
        }

        private void OnPrefabLoaded(AsyncOperationHandle<GameObject> obj)
        {
            obj.Completed -= OnPrefabLoaded;

            _storeItemPooler = new MonoBehaviourPooling<StoreItemView>(
                transform, obj.Result.GetComponent<StoreItemView>());

            OnStoreTabReady?.Invoke();
        }

    }

    public delegate void OnStoreTabReadyDelegate();

    public delegate bool IsItemLockedDelegate(IStoreObject item);
    public delegate bool IsItemPurchasableDelegate(IStoreObject item);
    public delegate bool IsItemButtonInteractableDelegate(IStoreObject item);
}