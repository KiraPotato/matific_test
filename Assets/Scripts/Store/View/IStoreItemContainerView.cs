﻿using System.Collections.Generic;

using TestProject.Data;
using TestProject.Store.Implementation;

namespace TestProject.Store
{
    public interface IStoreItemContainerView
    {
        event OnStoreObjectClickedDelegate OnStoreObjectClicked;
        event OnStoreTabReadyDelegate OnStoreTabReady;

        void Initialize(
            IsItemLockedDelegate isItemLockedCheck,
            IsItemPurchasableDelegate isItemPurchasableCheck, 
            IsItemButtonInteractableDelegate isItemButtonInteractableCheck);

        void RefreshButtons();
        void ShowEntries(IReadOnlyList<IStoreObject> entries);
    }
}