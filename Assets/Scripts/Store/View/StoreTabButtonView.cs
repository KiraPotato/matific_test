﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

namespace TestProject.Store.Implementation
{
    public class StoreTabButtonView : SerializedMonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private Button _button;
        [SerializeField] private TMP_Text _text;
#pragma warning restore 649

        private OnTabButtonClickedDelegate _onButtonClicked;
        private string _tabId;

        private void Awake()
        {
            if (NullChecker.IsAnyNull(_button, _text))
                Logger.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
        }

        public void Initialize(string tabId, OnTabButtonClickedDelegate onButtonClick)
        {
            _onButtonClicked = onButtonClick;
            _tabId = tabId;

            _button.onClick.RemoveListener(OnButtonClick);
            _button.onClick.AddListener(OnButtonClick);

            _text.text = tabId.ToUpper();
        }

        public void SetInteractable(bool isInteractable)
            => _button.interactable = isInteractable;

        private void OnButtonClick()
        {
            _onButtonClicked?.Invoke(_tabId);
        }
    }
}