﻿using System.Xml;
using System.Xml.Serialization;

using static Constants;

namespace TestProject.Data.Xml
{
    [XmlRoot(XML_STORE_ITEMS_ROOT)]
    public class StoreItemXmlCollection
    {
        [XmlElement(XML_ELEMENT_STORE_ITEMS)]
        public StoreXmlEntry[] StoreItems;
    }
}