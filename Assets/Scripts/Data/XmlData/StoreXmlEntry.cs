using System.Xml;
using System.Xml.Serialization;

using static Constants;

namespace TestProject.Data.Xml
{
    public class StoreXmlEntry
    {
        [XmlElement(XML_ELEMENT_NAME)]
        public string Name;

        [XmlElement(XML_ELEMENT_SECTION)]
        public string Section;

        [XmlElement(XML_ELEMENT_PRICE)]
        public int Price;

        [XmlElement(XML_ELEMENT_MIN_LEVEL)]
        public int MinLevel;

        [XmlElement(XML_ELEMENT_UI_ICON)]
        public string UiIcon;

        [XmlElement(XML_ELEMENT_GAME_SPRITE)]
        public string GameSprite;
    }
}