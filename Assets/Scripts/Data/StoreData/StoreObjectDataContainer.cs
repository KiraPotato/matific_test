﻿using System.Collections.Generic;

using TestProject.Data.Xml;

namespace TestProject.Data.Implementation
{
    public class StoreObjectDataContainer : IStoreObjectsContainer
    {
        public IReadOnlyDictionary<string, IReadOnlyList<StoreObjectData>> StoreObjects
            => _storeObjects;

        private Dictionary<string, IReadOnlyList<StoreObjectData>> _storeObjects;

        public StoreObjectDataContainer(string xmlText, bool preloadSprites = true)
        {
            Dictionary<string, List<StoreObjectData>> tempDict =
                new Dictionary<string, List<StoreObjectData>>();

            var collection = xmlText.ParseXML<StoreItemXmlCollection>();

            foreach (var item in collection.StoreItems)
            {
                if (tempDict.ContainsKey(item.Section) == false)
                    tempDict[item.Section] = new List<StoreObjectData>();

                tempDict[item.Section].Add(new StoreObjectData(item, preloadSprites));
            }

            _storeObjects = new Dictionary<string, IReadOnlyList<StoreObjectData>>();

            foreach (var item in tempDict)
                _storeObjects[item.Key] = item.Value;
        }
    }
}