﻿using UnityEngine;

namespace TestProject.Data
{
    public interface IStoreObject
    {
        int MinLevel { get; }
        string Name { get; }
        int Price { get; }
        string Section { get; }
        bool Owned { get; }

        void AsyncFetchIconSprite(OnSpriteReadyDelegate onIconLoaded = null);
        void AsyncFetchWorldSprite(OnSpriteReadyDelegate onWorldObjectLoaded = null);
        void ChangeOwnedState(bool isOwned);
    }

    public delegate void OnSpriteReadyDelegate(Sprite sprite);
}