using UnityEngine;
using UnityEngine.AddressableAssets;

using TestProject.Data.Xml;

namespace TestProject.Data.Implementation
{
    public class StoreObjectData : IStoreObject
    {
        public string Name { get; private set; }
        public string Section { get; private set; }
        public int Price { get; private set; }
        public int MinLevel { get; private set; }

        public bool Owned { get; private set; }

        private Sprite _icon;
        private Sprite _worldObject;
        private string _iconAddress;
        private string _worldObjectAddress;

        public StoreObjectData(StoreXmlEntry entry, bool preloadSprites = true)
        {
            Name = entry.Name;
            Section = entry.Section;
            Price = entry.Price;
            MinLevel = entry.MinLevel;

            Owned = Price == 0 && MinLevel == 0;

            _iconAddress = entry.UiIcon;
            _worldObjectAddress = entry.GameSprite;

            if (preloadSprites)
            {
                AsyncFetchIconSprite();
                AsyncFetchWorldSprite();
            }
        }

        public void AsyncFetchIconSprite(OnSpriteReadyDelegate onIconLoaded = null)
        {
            AsyncLoad(ref _icon, _iconAddress, (sprite) =>
            {
                _icon = sprite;
                onIconLoaded?.Invoke(_icon);
            });
        }

        public void AsyncFetchWorldSprite(
            OnSpriteReadyDelegate onWorldObjectLoaded = null)
        {
            AsyncLoad(ref _worldObject, _worldObjectAddress, (sprite) =>
            {
                _worldObject = sprite;
                onWorldObjectLoaded?.Invoke(sprite);
            });
        }

        public void ChangeOwnedState(bool isOwned)
            => Owned = isOwned;

        private void AsyncLoad(
            ref Sprite localSprite, 
            string address, 
            OnSpriteReadyDelegate callback)
        {
            if (localSprite != null)
            {
                callback?.Invoke(localSprite);
                return;
            }

            Addressables.LoadAssetAsync<Sprite>(address).Completed += 
                (i) => callback?.Invoke(i.Result);
        }
    }
}