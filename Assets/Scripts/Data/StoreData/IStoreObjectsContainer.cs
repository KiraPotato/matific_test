﻿using System.Collections.Generic;

using TestProject.Data.Implementation;

namespace TestProject.Data
{
    public interface IStoreObjectsContainer
    {
        IReadOnlyDictionary<string, IReadOnlyList<StoreObjectData>> StoreObjects { get; }
    }
}