﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

public static class XmlParser
{
    public static Stream ToStream(this string @this)
    {
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(@this);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }

    public static TOutput ParseXML<TOutput>(this string @this) where TOutput : class
    {
        XmlReaderSettings settings = new XmlReaderSettings()
        {
            ConformanceLevel = ConformanceLevel.Document
        };

        Stream input = @this.Trim().ToStream();

        XmlReader reader = XmlReader.Create(input, settings);
        TOutput output = new XmlSerializer(typeof(TOutput)).Deserialize(reader) as TOutput;

        input.Dispose();

        return output;
    }
}
