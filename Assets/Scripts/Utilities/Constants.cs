﻿public static class Constants
{
    public const string ERROR_MISSING_EDITOR_REFERENCES = "Some editor references were not set in editor";

    public const string XML_STORE_ITEMS_ROOT = "items";
    public const string XML_ELEMENT_STORE_ITEMS = "item";
    public const string XML_ELEMENT_NAME = "name";
    public const string XML_ELEMENT_SECTION = "section";
    public const string XML_ELEMENT_PRICE = "price";
    public const string XML_ELEMENT_MIN_LEVEL = "min_level";
    public const string XML_ELEMENT_UI_ICON = "ui_icon";
    public const string XML_ELEMENT_GAME_SPRITE = "game_sprite";
}
