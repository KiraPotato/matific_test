﻿using UnityEngine;

public static class CanvasGroupUtilities
{
    private const float DISABLED_VALUE = 0f;
    private const float ENABLED_VALUE = 1f;

    public static void Show(this CanvasGroup canvasGroup) => canvasGroup.alpha = ENABLED_VALUE;

    public static void Hide(this CanvasGroup canvasGroup) => canvasGroup.alpha = DISABLED_VALUE;

    public static void ChangeVisibility(this CanvasGroup canvasGroup, bool isVisible)
    {
        if (isVisible)
            canvasGroup.Show();
        else
            canvasGroup.Hide();
    }
}
