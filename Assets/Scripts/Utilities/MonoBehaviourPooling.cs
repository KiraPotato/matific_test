﻿using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviourPooling<TObjectType> 
    where TObjectType : MonoBehaviour
{
    private Stack<TObjectType> _pooledObjects;
    private Transform _parent;
    private TObjectType _prefab;

    public MonoBehaviourPooling(Transform parent, TObjectType prefab)
    {
        _parent = parent;
        _prefab = prefab;
        _pooledObjects = new Stack<TObjectType>();
    }

    public TObjectType FetchObject(string newObjectName)
    {
        TObjectType obj;

        if (_pooledObjects.Count == 0)
        {
            obj = Object.Instantiate(_prefab, _parent);
        }
        else
        {
            obj = _pooledObjects.Pop();
            obj.transform.SetParent(_parent);
        }

        obj.name = newObjectName;
        obj.gameObject.SetActive(true);

        return obj;
    }

    public void PoolObject(TObjectType obj)
    {
        obj.gameObject.SetActive(false);
        _pooledObjects.Push(obj);
    }
}