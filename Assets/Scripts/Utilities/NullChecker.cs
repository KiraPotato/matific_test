﻿public static class NullChecker
{
    public static bool IsNull(object obj)
        => obj == null || obj.Equals(null);

    public static bool IsNotNull(object obj)
        => IsNull(obj) == false;

    public static bool IsAnyNull(params object[] objects)
    {
        if (objects.Length == 0)
            return false;
        else if (objects.Length == 1)
            return IsNull(objects[0]);
        else
        {
            foreach (object obj in objects)
            {
                if (IsNull(obj))
                    return true;
            }
        }

        return false;
    }

    public static bool AreNoneNull(params object[] objects)
        => IsAnyNull(objects) == false;
}