﻿using UnityEngine;

public static class Logger
{
    private const int NORMAL_LOG_STACK_INDEX = 3;
    private const int FORMATTED_LOG_STACK_INDEX = 4;

    private const string MESSAGE_FORMAT = "{0} :: {1} :: {2}";

    private const string STACK_TRACE_MESSAGE_FORMAT = "Printing stack trace for {0} frames with message: {1}\n";
    private const string STACK_TRACE_MESSAGE_BLANK_FORMAT = "Printing stack trace for {0} frames\n";
    private const string STACK_TRACE_LINE_FORMAT = "\n Frame {0} - {1} :: {2}";
    private const string DOUBLE_NEW_LINE = "\n\n";

    private const int TRACE_START_INDEX = 2;
    private const int TRACE_EXTRA_FRAMES = 3;

    #region Info
    public static void LogMessage(in string message)
        => DoLog(message, LogType.Info, NORMAL_LOG_STACK_INDEX, null);

    public static void LogMessage(in string message, Object context)
        => DoLog(message, LogType.Info, NORMAL_LOG_STACK_INDEX, context);

    public static void LogMessageFormat(in string message, params string[] arguments)
        => DoLogFormat(message, null, LogType.Info, arguments);

    public static void LogMessageFormat(in string message, Object context, params string[] arguments)
        => DoLogFormat(message, context, LogType.Info, arguments);

    public static void PrintStackTrace(int framesCount, in string message)
        => PrintStackTrace(framesCount, message, null);

    public static void PrintStackTrace(int framesCount, in string message, Object context)
    {
        if (framesCount <= 0)
            return;

        string stackMessage;

        if (string.IsNullOrWhiteSpace(message) == false)
            stackMessage = string.Format(STACK_TRACE_MESSAGE_FORMAT, framesCount, message);
        else
            stackMessage = string.Format(STACK_TRACE_MESSAGE_BLANK_FORMAT, framesCount);

        for (int frameIndex = TRACE_START_INDEX; frameIndex < framesCount + TRACE_EXTRA_FRAMES; frameIndex++)
        {
            if (TryFetchFrameData(frameIndex, out string className, out string methodName))
            {
                int index = frameIndex - TRACE_START_INDEX;
                stackMessage += string.Format(STACK_TRACE_LINE_FORMAT, index, className, methodName);
            }
        }

        PrintLog(stackMessage, LogType.Info, context);
    }
    #endregion

    #region Warning message
    public static void LogWarning(in string message)
        => DoLog(message, LogType.Warning, NORMAL_LOG_STACK_INDEX, null);

    public static void LogWarning(in string message, Object context)
        => DoLog(message, LogType.Warning, NORMAL_LOG_STACK_INDEX, context);

    public static void LogWarningFormat(in string message, Object context, params string[] arguments)
        => DoLogFormat(message, context, LogType.Warning, arguments);

    public static void LogWarningFormat(in string message, params string[] arguments)
        => DoLogFormat(message, null, LogType.Warning, arguments);
    #endregion

    #region Error Message
    public static void LogError(in string message)
        => DoLog(message, LogType.Error, NORMAL_LOG_STACK_INDEX, null);

    public static void LogError(in string message, Object context)
        => DoLog(message, LogType.Error, NORMAL_LOG_STACK_INDEX, context);

    public static void LogErrorFormat(in string message, Object context, params string[] arguments)
        => DoLogFormat(message, context, LogType.Error, arguments);

    public static void LogErrorFormat(in string message, params string[] arguments)
        => DoLogFormat(message, null, LogType.Error, arguments);
    #endregion

    #region Print handlers
    private static void DoLogFormat(in string message, Object context, LogType logType, params string[] arguments)
    {
        string finalMessage = string.Format(message, arguments);

        DoLog(finalMessage, logType, FORMATTED_LOG_STACK_INDEX, context);
    }

    private static void DoLog(in string message, LogType logType, int stackIndex, Object context)
    {
        string finalMessage;

        if (TryFetchFrameData(stackIndex, out string className, out string methodName))
            finalMessage = string.Format(MESSAGE_FORMAT, className, methodName, message);
        else
            finalMessage = message;

        PrintLog(finalMessage, logType, context);
    }

    private static void PrintLog(in string message, LogType logType, Object context)
    {
        string printedMessage = message + DOUBLE_NEW_LINE;

        switch (logType)
        {
            case LogType.Info:
                Debug.Log(printedMessage, context);
                break;

            case LogType.Warning:
                Debug.LogWarning(printedMessage, context);

                break;
            case LogType.Error:
                Debug.LogError(printedMessage, context);
                break;
        }
    }
    #endregion

    public static bool TryFetchFrameData(int targetFrame, out string className, out string methodName)
    {
        try
        {
            System.Diagnostics.StackTrace stack = new System.Diagnostics.StackTrace();
            System.Diagnostics.StackFrame frame = stack.GetFrame(targetFrame);

            methodName = frame.GetMethod().Name;
            className = frame.GetMethod().DeclaringType.ToString();

            return true;
        }
        catch (System.Exception)
        {
            className = default;
            methodName = default;

            return false;
        }
    }

    private enum LogType
    {
        Info,
        Warning,
        Error
    }
}
